<?php include 'header.php';?>
<nav class="breadcrumb">
  <ol class="breadcrumb" style="width:60;">
    <img src="kaodim.png" height="80" alt="kaodimlogo">
  </ol>
</nav>
<div class="p-3 m-2 bg-light">
    <div class="d-flex justify-content-center">
        <div class="card border-dark mb-3 p-3" style="width: 40rem;" >
            <div class="card-header text-center">
                Cleaning Service Request Form
            </div>
            <div class="card-body">
                <form action="submit_request.php" method="post">
                    <div class="form-group">
                        <label for="exampleLocation">Location</label><br />
                        <select class="form-control" required name="location">
                            <option value="">-- Select Location --</option>
                            <option value="Petaling Jaya">Petaling Jaya</option>
                            <option value="Subang Jaya">Subang Jaya</option>
                            <option value="Puchong">Puchong</option>
                            <option value="Mont Kiara">Mont Kiara</option>
                            <option value="Kepong">Kepong</option>
                            <option value="Shah Alam">Shah Alam</option>
                            <option value="Cheras">Cheras</option>
                            <option value="Sentul">Sentul</option>
                            <option value="Setiawangsa">Setiawangsa</option>
                            <option value="TTDI">TTDI</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" name="email" placeholder="example@email.com" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDate">Date</label>
                        <input type="date" class="form-control" name="date" required>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" required>
                        <label class="form-check-label" for="exampleCheck1">Please tick for confirmation.</label>
                    </div>
                    <div class="text-center">
                    <button type="submit" class="btn btn-danger" style="center">Sign Up</button>
                    </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<?php include 'footer.php';?>