CREATE DATABASE IF NOT EXISTS `kaodim_database`;

USE `kaodim_database`;

CREATE TABLE `job_requests` (
    `id` int NOT NULL AUTO_INCREMENT,
    `location` varchar(255),
    `email` varchar(255),
    `date` varchar(255),
    `vendor_id` int,
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `vendors` (
    `id` int NOT NULL AUTO_INCREMENT,
    `name` varchar(255),
    `location` varchar(255),
    `availability` boolean,
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `vendors` (`name`, `location`, `availability`)
    VALUES 
        ('James', 'Petaling Jaya', 1),
        ('Johan', 'Mont Kiara', 1),
        ('Jayden', 'Kepong', 1),
        ('Kristy', 'Sri Petalig', 1),
        ('Selina', 'TTDI', 1),
        ('Jordan', 'Subang Jaya', 1);