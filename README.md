# Kaodim Interview Assignment

## Prerequsites
- Install XAMPP via https://www.apachefriends.org/index.html
- Configure `MySQL` config according your local credentials in `XAMPP` 
- Start `Apache` and `MySQL` server in `XAMPP`

## Setup
1. `cd /path/to/XAMPP/htdocs/`
2. `git clone https://gitlab.com/kiritoxbeng/kaodim-interview-assignment.git`
3. In command line, run `mysql -u <USERNAME> -p <PASSWORD>` (replace `<USERNAME>` and `<PASSWORD>` according to your local setup)
4. Run `source /path/to/repo/folder/init.sql`
5. Run `exit`

## View
1. Open http://localhost/kaodim-interview-assignment once the server started

## How it works
1. User requires to fill up the form before they signup.
2. Once the user signup successfully, the information of the user will store into the SQL database.